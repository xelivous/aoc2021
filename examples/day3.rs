use aoc2021::day3;

fn main() {
    let input = include_str!("../inputs/day3.txt");
    eprintln!("Part 1: {}", day3::part1(&input));
    eprintln!("Part 2: {}", day3::part2(&input));
}
