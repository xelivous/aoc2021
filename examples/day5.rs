use aoc2021::day5;
use aoc2021::input_to_vec;

fn main() {
    let input = input_to_vec("inputs/day5.txt");
    eprintln!("Part 1: {}", day5::find_intersections(&input, false));
    eprintln!("Part 2: {}", day5::find_intersections(&input, true));
}
