use aoc2021::day4;

fn main() {
    let input = include_str!("../inputs/day4.txt");
    eprintln!("Part 1: {}", day4::part1(&input));
    eprintln!("Part 2: {}", day4::part2(&input));
}
