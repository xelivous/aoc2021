use aoc2021::{day2, input_to_vec};

fn main() {
    let input = input_to_vec("inputs/day2.txt");
    eprintln!("Part 1: {}", day2::part1(&input));
    eprintln!("Part 2: {}", day2::part2(&input));
}
