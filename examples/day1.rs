use aoc2021::{day1, input_to_vec};

fn main() {
    let input = input_to_vec("inputs/day1.txt");
    eprintln!("Part 1: {}", day1::part1(&input));
    eprintln!("Part 2: {}", day1::part2(&input));
}
