// https://adventofcode.com/2021/day/4
use std::{collections::HashMap, fmt::Display, str::FromStr};

// It's possible to find the board X/Y from the dataset, but it specified 5x5 so just const it
const BOARD_X: usize = 5;
const BOARD_Y: usize = 5;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Game {
    calls: Vec<usize>,
    boards: Vec<Board>,
}
impl FromStr for Game {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let data: Vec<_> = s.split("\n\n").collect();
        let calls = data[0]
            .split(',')
            .map(|x| x.parse::<usize>().unwrap())
            .collect();
        let boards = data[1..]
            .iter()
            .map(|x| x.parse::<Board>().unwrap())
            .collect();

        Ok(Self { calls, boards })
    }
}
impl Game {
    pub fn check_winner(&self) -> Option<&Board> {
        for board in &self.boards {
            if board.check_for_win() {
                return Some(&board);
            }
        }

        None
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Board {
    /// Stores the values at a specific position.
    data: Vec<usize>,
    /// A reverse lookup for the data vec,
    /// to be able to quickly find if a value is on this board and where it is
    lookup: HashMap<usize, usize>,
    /// A vec to determine if a specific tile is called or not
    called: Vec<bool>,
}
impl FromStr for Board {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut data = Vec::with_capacity(BOARD_X * BOARD_Y);
        let mut lookup = HashMap::new();

        let cells: Vec<Vec<&str>> = s.lines().map(|l| l.split_whitespace().collect()).collect();

        for (y, cell) in cells.iter().enumerate() {
            for (x, val) in cell.iter().enumerate() {
                let val = val.parse::<usize>().unwrap();
                data.push(val);
                lookup.insert(val, x + y * BOARD_X);
            }
        }

        Ok(Self {
            data,
            lookup,
            called: vec![false; BOARD_X * BOARD_Y],
        })
    }
}
// Implement display for easier debugging of what is not called
impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output: Vec<String> = vec![];
        for y in 0..BOARD_Y {
            let mut line: Vec<String> = vec![];
            for x in 0..BOARD_Y {
                if self.called[x + y * BOARD_Y] {
                    line.push(format!("  X"));
                } else {
                    line.push(format!("{:3}", self.data[(x + y * BOARD_Y)]));
                }
            }
            output.push(line.join(""));
        }
        f.write_str(&output.join("\n"))
    }
}

impl Board {
    /// Mark a specific tile as called if it exists on the board
    pub fn call(&mut self, val: usize) {
        match self.lookup.get(&val) {
            Some(v) => self.called[*v] = true,
            None => {}
        }
    }

    /// Checks if this board has a win horizontally or vertically
    pub fn check_for_win(&self) -> bool {
        self.check_for_horz_win() || self.check_for_vert_win()
    }

    /// traverse through the board vertically, checking if an entire row is filled
    pub fn check_for_horz_win(&self) -> bool {
        for y in 0..BOARD_Y {
            let offset = y * BOARD_X;
            match self.called[offset..offset + BOARD_X]
                .iter()
                .filter(|x| **x)
                .count()
                == BOARD_X
            {
                true => return true,
                false => continue,
            }
        }

        false
    }

    /// traverse through the board horizontally, checking if an entire column is filled
    pub fn check_for_vert_win(&self) -> bool {
        for x in 0..BOARD_X {
            let mut valid = 0;
            for y in 0..BOARD_Y {
                if self.called[x + y * BOARD_Y] {
                    valid += 1;
                }
            }

            if valid == BOARD_Y {
                return true;
            }
        }

        false
    }

    /// Returns the sum of all of the uncalled numbers
    pub fn score(&self) -> usize {
        let mut score = 0;
        for (idx, call) in self.called.iter().enumerate() {
            if call == &false {
                score += self.data[idx];
            }
        }

        score
    }
}

pub fn part1(input: &str) -> usize {
    let mut game: Game = input.parse().unwrap();

    for call in &game.calls {
        for board in &mut game.boards {
            board.call(*call);
        }

        if let Some(winner) = game.check_winner() {
            return winner.score() * call;
        }
    }

    0
}

pub fn part2(input: &str) -> usize {
    let mut game: Game = input.parse().unwrap();

    for call in &game.calls {
        let mut i = 0;
        while i < game.boards.len() {
            game.boards[i].call(*call);

            if game.boards[i].check_for_win() {
                if game.boards.len() == 1 {
                    return game.boards[i].score() * call;
                } else {
                    game.boards.remove(i);
                }
            } else {
                i += 1;
            }
        }
    }

    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_for_horz_win() {
        let input = include_str!("../inputs/day4_test1.txt");
        let mut game: Game = input.parse().unwrap();
        let board = &mut game.boards[0];
        assert_eq!(board.check_for_horz_win(), false);
        board.call(22);
        board.call(13);
        board.call(17);
        board.call(11);
        assert_eq!(board.check_for_horz_win(), false);
        board.call(0);
        assert_eq!(board.check_for_horz_win(), true);

        let board = &mut game.boards[1];
        assert_eq!(board.check_for_horz_win(), false);
        board.call(9);
        board.call(18);
        board.call(13);
        board.call(17);
        assert_eq!(board.check_for_horz_win(), false);
        board.call(5);
        assert_eq!(board.check_for_horz_win(), true);

        let board = &mut game.boards[2];
        assert_eq!(board.check_for_horz_win(), false);
        board.call(14);
        board.call(21);
        board.call(17);
        board.call(24);
        assert_eq!(board.check_for_horz_win(), false);
        board.call(4);
        assert_eq!(board.check_for_horz_win(), true);
    }

    #[test]
    fn check_for_vert_win() {
        let input = include_str!("../inputs/day4_test1.txt");
        let mut game: Game = input.parse().unwrap();

        let board = &mut game.boards[0];
        assert_eq!(board.check_for_vert_win(), false);
        board.call(22);
        board.call(8);
        board.call(21);
        board.call(6);
        assert_eq!(board.check_for_vert_win(), false);
        board.call(1);
        assert_eq!(board.check_for_vert_win(), true);

        let board = &mut game.boards[1];
        assert_eq!(board.check_for_vert_win(), false);
        board.call(15);
        board.call(18);
        board.call(8);
        board.call(11);
        assert_eq!(board.check_for_vert_win(), false);
        board.call(21);
        assert_eq!(board.check_for_vert_win(), true);
    }

    #[test]
    fn test_parts() {
        let input = include_str!("../inputs/day4_test1.txt");
        assert_eq!(part1(&input), 4512);
        assert_eq!(part2(&input), 1924);

        let input = include_str!("../inputs/day4.txt");
        assert_eq!(part1(&input), 34506);
        assert_eq!(part2(&input), 7686);
    }
}
