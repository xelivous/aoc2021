// https://adventofcode.com/2021/day/5

use std::{collections::HashMap, str::FromStr};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Vector2 {
    x: isize,
    y: isize,
}
impl FromStr for Vector2 {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) = s.split_once(",").unwrap();
        let x = x.parse().unwrap();
        let y = y.parse().unwrap();
        Ok(Self { x, y })
    }
}

#[derive(Debug)]
pub struct Line {
    start: Vector2,
    end: Vector2,
}
impl FromStr for Line {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (start, end) = s.split_once(" -> ").unwrap();
        let start = start.parse()?;
        let end = end.parse()?;
        Ok(Self { start, end })
    }
}
impl Line {
    pub fn to_point_list(&self, allow_diag: bool) -> Vec<Vector2> {
        let mut points = vec![];

        let mut distancey = self.end.y - self.start.y;
        let mut distancex = self.end.x - self.start.x;

        // early exit if we don't support diagonals
        if !allow_diag && (distancex != 0 && distancey != 0) {
            return points;
        }

        while distancex != 0 || distancey != 0 {
            let x = self.start.x + distancex;
            let y = self.start.y + distancey;

            points.push(Vector2 { x, y });

            if distancex > 0 {
                distancex -= 1;
            } else if distancex < 0 {
                distancex += 1;
            }
            if distancey > 0 {
                distancey -= 1;
            } else if distancey < 0 {
                distancey += 1;
            }
        }

        points.push(self.start);

        points
    }
}

pub fn find_intersections(input: &Vec<Line>, allow_diag: bool) -> usize {
    let mut map = HashMap::new();
    for line in input {
        for point in line.to_point_list(allow_diag) {
            if let Some(v) = map.get_mut(&point) {
                *v += 1;
            } else {
                map.insert(point, 1);
            }
        }
    }

    map.iter().filter(|(_pos, val)| val > &&1).count()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::input_to_vec;

    #[test]
    fn example1() {
        let input = input_to_vec("inputs/day5_test1.txt");
        assert_eq!(find_intersections(&input, false), 5);
        assert_eq!(find_intersections(&input, true), 12);
    }

    #[test]
    fn point_list() {
        let line: Line = "1,1 -> 1,3".parse().unwrap();
        assert_eq!(
            line.to_point_list(true),
            vec![
                Vector2 { x: 1, y: 3 },
                Vector2 { x: 1, y: 2 },
                Vector2 { x: 1, y: 1 },
            ]
        );

        let line: Line = "9,7 -> 7,7".parse().unwrap();
        assert_eq!(
            line.to_point_list(true),
            vec![
                Vector2 { x: 7, y: 7 },
                Vector2 { x: 8, y: 7 },
                Vector2 { x: 9, y: 7 },
            ]
        );

        let line: Line = "1,1 -> 3,3".parse().unwrap();
        assert_eq!(
            line.to_point_list(true),
            vec![
                Vector2 { x: 3, y: 3 },
                Vector2 { x: 2, y: 2 },
                Vector2 { x: 1, y: 1 },
            ]
        );

        let line: Line = "9,7 -> 7,9".parse().unwrap();
        assert_eq!(
            line.to_point_list(true),
            vec![
                Vector2 { x: 7, y: 9 },
                Vector2 { x: 8, y: 8 },
                Vector2 { x: 9, y: 7 },
            ]
        );
    }

    #[test]
    fn more_points() {
        let line: Line = "6,4 -> 2,0".parse().unwrap();
        assert_eq!(
            line.to_point_list(true),
            vec![
                Vector2 { x: 2, y: 0 },
                Vector2 { x: 3, y: 1 },
                Vector2 { x: 4, y: 2 },
                Vector2 { x: 5, y: 3 },
                Vector2 { x: 6, y: 4 },
            ]
        );
    }
}
