// https://adventofcode.com/2021/day/1

pub fn part1(input: &Vec<usize>) -> usize {
    input.windows(2).filter(|data| data[1] > data[0]).count()
}

pub fn part2(input: &Vec<usize>) -> usize {
    input.windows(4).filter(|data| data[3] > data[0]).count()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::input_to_vec;

    #[test]
    fn example1() {
        let input = input_to_vec("inputs/day1_test1.txt");
        assert_eq!(part1(&input), 7);
    }
}
