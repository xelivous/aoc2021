// https://adventofcode.com/2021/day/3

use std::{num::ParseIntError, str::FromStr};

use crate::str_to_vec;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Binary(usize);
impl FromStr for Binary {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Binary(usize::from_str_radix(s, 2)?))
    }
}

pub fn part1(input: &str) -> usize {
    let numbers: Vec<Binary> = str_to_vec(input);
    let line_width = input.lines().collect::<Vec<_>>().get(0).unwrap().len();

    let mut gamma = 0;
    let mut epsilon = 0;

    for x in (0..line_width).rev() {
        let zeros = numbers.iter().filter(|v| (v.0 >> x) & 1 != 0).count();
        let ones = numbers.len() - zeros;

        if ones >= zeros {
            gamma += 1 << x;
        } else {
            epsilon += 1 << x;
        }
    }

    epsilon * gamma
}

pub fn part2(input: &str) -> usize {
    let numbers: Vec<Binary> = str_to_vec(input);
    // get the width of a line to help with bitshifting to get the leftmost bit
    let width = input.lines().collect::<Vec<_>>().get(0).unwrap().len() - 1;

    let oxy = part2_find_num(&numbers, width, 1, 0);
    let scrub = part2_find_num(&numbers, width, 0, 1);

    oxy * scrub
}

fn part2_find_num(data: &Vec<Binary>, width: usize, target: usize, exclude: usize) -> usize {
    let mut arr = data.clone();
    let mut pos = width;

    while arr.len() > 1 {
        let ones = arr.iter().filter(|v| (v.0 >> pos) & 1 != 0).count();
        let zeros = arr.len() - ones;

        let filter = if ones > zeros {
            target
        } else if ones == zeros {
            target
        } else {
            exclude
        };

        // drain the vec if it doesn't match our filter
        let mut i = 0;
        while i < arr.len() && arr.len() > 1 {
            if (arr[i].0 >> pos) & 1 != filter {
                arr.remove(i);
            } else {
                i += 1;
            }
        }

        pos = pos.saturating_sub(1);
    }

    arr[0].0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1() {
        let input = include_str!("../inputs/day3_test1.txt");
        assert_eq!(part1(&input), 198);
        assert_eq!(part2(&input), 230);
    }
}
