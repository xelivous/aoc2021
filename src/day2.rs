// https://adventofcode.com/2021/day/2

use std::str::FromStr;

#[derive(Debug)]
pub struct Commands {
    direction: Direction,
    amount: isize,
}
impl FromStr for Commands {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let data: Vec<_> = s.split_whitespace().collect();
        let direction = data[0].parse()?;
        let amount = data[1].parse().map_err(|_| "can't parse amount")?;

        Ok(Self { direction, amount })
    }
}

#[derive(Debug)]
pub enum Direction {
    Forward,
    Down,
    Up,
}
impl FromStr for Direction {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "forward" => Ok(Self::Forward),
            "down" => Ok(Self::Down),
            "up" => Ok(Self::Up),
            _ => Err("bad input"),
        }
    }
}

pub fn part1(input: &Vec<Commands>) -> isize {
    let (mut horz, mut depth) = (0, 0);

    for command in input {
        match command.direction {
            Direction::Forward => horz += command.amount,
            Direction::Down => depth += command.amount,
            Direction::Up => depth -= command.amount,
        }
    }
    horz * depth
}

pub fn part2(input: &Vec<Commands>) -> isize {
    let (mut horz, mut depth, mut aim) = (0, 0, 0);

    for command in input {
        match command.direction {
            Direction::Forward => {
                horz += command.amount;
                depth += aim * command.amount;
            }
            Direction::Down => aim += command.amount,
            Direction::Up => aim -= command.amount,
        }
    }
    horz * depth
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::input_to_vec;

    #[test]
    fn example1() {
        let input = input_to_vec("inputs/day2_test1.txt");
        assert_eq!(part1(&input), 150);
        assert_eq!(part2(&input), 900);
    }
}
