use std::fs::read_to_string;
use std::path::Path;
use std::str::FromStr;

pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;

pub fn input_to_vec<T, P>(input: P) -> Vec<T>
where
    P: AsRef<Path>,
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Debug,
{
    let data = read_to_string(input).expect("input file missing");
    data.lines()
        .map(|x| x.parse::<T>().expect("unable to parse line"))
        .collect()
}

pub fn str_to_vec<T>(input: &str) -> Vec<T>
where
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Debug,
{
    input
        .lines()
        .map(|x| x.parse::<T>().expect("unable to parse line"))
        .collect()
}
