# AOC2021

* inputs thrown into `./inputs/`
* implementations thrown into mods in `./src/` along with the examples
* day outputs thrown into `./examples/`

```sh
cargo run --example day1
```
